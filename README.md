# Simulated Omni Drive Skill

The Simulated Omni Drive Skill is a simple skill that simulates the movement performed by omnidirectional robots. It is possible to define four different goals for this skill.

| Parameter | Description                                                   |
|-----------|---------------------------------------------------------------|
| x:        | Final desired position on X axis.                             |
| y:        | Final desired position on Y axis.                             |
| ori:      | Final orientation in radians.                                 |
| speed:    | Speed at which the robot moves from starting to final points. |

## Table of Contents

* [Installation](#installation)
* [Usage](#usage)
* [Continuous Integration/Deployment](#cicd)

## <a name="installation"></a>Installation

1. Download the [simulated_omni_drive_skill]() repository to the src folder of your catkin workspace.

2. Build your code. Assuming your catkin workspace is located in ~/catkin_ws:

    ```bash
    cd ~/catkin_ws
    catkin_make
    ```

## <a name="usage"></a> Usage

To initiate the Simulated Omni Drive Skill Server, perform:

```bash
roslaunch simulated_omni_drive_skill_server run.launch
```

## <a name="cicd"></a> Continuous Integration/Deployment

The main focus of this repository is to exemplify the usage of our CI/CD pipeline in robotics, using AWS RoboMaker as well as other AWS Services.

### .gitlab-ci.yml

In the root of this repository you can find an example CI/CD configuration file for this Skill in particular. Various variables are needed for the correct execution and use of this tool:

* **AWS:** Must be true if AWS simulation wants to be used. False otherwise.
* **AWS_ACCESS_KEY:** AWS Access Key belonging to the User.
* **AWS_SECRET_KEY:** AWS Secret Key belonging to the User.
* **AWS_REGION:** Region where your services to be executed are located
* **AWS_ROBOT_BUNDLE:** Path to Robot bundle file, of type: "s3://example/my-robot-application.tar.gz"
* **AWS_SIM_FUNCTION:** Name of the AWS Lambda Function responsible for the execution of all the process.
* **AWS_SF_ARN:** ARN of the AWS Step Functions State Machine responsible for the monitorization of the simulation state.

If additionally the deployment of this repository to a Docker image is desired, the "deployment_job" tag and its contents must be uncommented and the registries/access information changed accordingly.

### .ci_scripts

In the root of this repository you can also find an example of a folder containing a configuration script and a simulation script.

* **aws_init.sh:** should most of the cases be left as is. It sets up all AWS related configurations.
* **aws_simulation.sh:** should be modified for each different case. It's responsible for the execution and monitorization of the end of the simulation based on the state of the AWS Step Functions State Machine used.


