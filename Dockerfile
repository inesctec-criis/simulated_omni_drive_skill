FROM ubuntu:latest as intermediate

# Install "git"
RUN apt-get update
RUN apt-get install -y \
    git

# Add credentials
ARG SSH_PRIVATE_KEY
RUN mkdir -p /root/.ssh/
RUN echo "${SSH_PRIVATE_KEY}" > /root/.ssh/gitlab_key
RUN chmod 600 /root/.ssh/gitlab_key
#COPY ./docker_config/config /root/.ssh/config
#RUN chmod 600 /root/.ssh/config

# Make sure domains are accepted
RUN touch /root/.ssh/known_hosts
RUN ssh-keyscan github.com >> /root/.ssh/known_hosts
RUN ssh-keyscan gitlab.inesctec.pt >> /root/.ssh/known_hosts

# Copy .rosintall file into workspace
COPY .rosinstall /ros_workspace/src/.rosinstall

# Fetch skill private dependencies
WORKDIR /ros_workspace/
#RUN wstool update -t src

################################################################################################################################################
################################################################################################################################################

FROM docker-registry.inesctec.pt/osps/docker-images/ros-melodic

# Copy skill dependencies from previous stage
COPY --from=intermediate /ros_workspace/src /ros_workspace/src/

# Copy skill's ROS packages
COPY ./simulated_omni_drive_skill_client   /ros_workspace/src/simulated_omni_drive_skill/simulated_omni_drive_skill_client/
COPY ./simulated_omni_drive_skill_msgs     /ros_workspace/src/simulated_omni_drive_skill/simulated_omni_drive_skill_msgs/
COPY ./simulated_omni_drive_skill_scxml    /ros_workspace/src/simulated_omni_drive_skill/simulated_omni_drive_skill_scxml/
COPY ./simulated_omni_drive_skill_server   /ros_workspace/src/simulated_omni_drive_skill/simulated_omni_drive_skill_server/

# Install all required dependencies and compile catkin workspace
RUN apt-get update \
    && pip install requests \
    && /bin/bash -c '. /opt/ros/${ROS_DISTRO}/setup.bash; cd /ros_workspace; rosdep install --from-paths src --ignore-src -r -y' \
    && /bin/bash -c '. /opt/ros/${ROS_DISTRO}/setup.bash; cd /ros_workspace; catkin config --install; catkin build' \
    && /bin/bash -c '. /ros_workspace/install/setup.bash; rospack profile' \
    && echo "source /ros_workspace/install/setup.bash" >> /root/.bashrc

# Start task manager
CMD /bin/bash -c 'source /ros_workspace/install/setup.bash && roslaunch simulated_omni_drive_skill_server run.launch'
